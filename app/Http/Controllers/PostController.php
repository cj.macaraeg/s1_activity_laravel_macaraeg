<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Access the authenticated user via the Auth Facade
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;

class PostController extends Controller
{
    public function create()
    {
        // action to return a view containing a form for blog post create
        return view('posts.create');
    }


    public function store(Request $request)
    {
        // if there is an authenticated user
        if(Auth::user()){
            // instantiate a new Post object from the Post Model
            $post = new Post;

            // define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key user_id of the new post
            $post->user_id = (Auth::user()->id);
            // save this post object in the database
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // action that will return a view showing all blog posts
    public function index()
    {
        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }

    // action for showing only the posts authored by authenticated user
    public function myPosts()
    {
        if(Auth::user()) {
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

     public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    // action that will return a view showing an edit form for specific post using the URL parameter $id to query for the database entry to be edited
    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    // action for updating the post
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');

            $post->save();
        }

        return redirect('/posts');
    }

    // action for deleting a post
    public function destroy($id)
    {
        $post = Post::find($id);

        //if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->delete();    
    }
    
        return redirect('/posts');
    }



    public function like($id){
    $post = Post::find($id);
    $user_id = Auth::user()->id;
    //if authenticated user is not the post author
    if($post->user_id != $user_id){
        //check if a post like has been made by this user before
        if($post->likes->contains("user_id", $user_id)){
            //delete the like made by this user to unlike this post
            PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
        }else{
            //create a new like record to like this post
            //instantiate a new PostLike object from the PostLike model
            $postLike = new PostLike;
            //define the properties of the $postLike object
            $postLike->post_id = $post->id;
            $postLike->user_id = $user_id;
            
            //save this postLike object in the database
            $postLike->save();
        }           
        return redirect("/posts/$id");
        }
    }

}